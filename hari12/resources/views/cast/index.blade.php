@extends('adminlte.master')
@section('content')
<div class="ml-3 mt-3">
<div class="card">
    <div class="card-header">
      <h3 class="card-title">Cast Table</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
      @if (session('success'))
          <div class="alert alert-success">
            {{( session ('success'))}}
          </div>
      @endif
      <table class="table table-bordered">
        <thead>
          <tr>
            <th>No</th>
            <th style="width: 200px">Nama Pemeran</th>
            <th>Umur</th>
            <th style="width: 600px">Bio</th>
            <th style="width: 120px">Actions</th>
          </tr>
        </thead>
        <tbody>
          @forelse ($casts as $key => $cast)
            <tr>
              <td> {{ $key + 1 }} </td>
              <td> {{ $cast->nama }} </td>
              <td> {{ $cast->umur }} </td>
              <td> {{ $cast->bio }} </td>
              <td style="display : flex;"> 
                <a href="/cast/{{$cast->id}}" class="btn btn-info btn-sm"> Lihat </a>
                <a href="/cast/{{$cast->id}}/edit" class="btn btn-default btn-sm"> Edit </a>
                <form action="/cast/{{ $cast->id }}" method="post">
                  @csrf
                  @method('DELETE')
                  <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                </form>
              </td>
            </tr>
            @empty
              <tr>
                <td colspan="4" align="center"> Tidak Ada Daftar Cast </td>
              </tr>
          @endforelse
        </tbody>
      </table>
      <a class="btn btn-info mt-4" href="/cast/create">
        Daftar Baru Pemain
      </a>
    </div>
    <!-- /.card-body -->
  </div>
</div>


@endsection