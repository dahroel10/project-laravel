<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Validator;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@home');

Route::get('/register', 'AuthController@register');

Route::get('/welcome', 'AuthController@welcome');
Route::post('/welcome', 'AuthController@welcome');

Route::get('/master', 'HomeController@master');

Route::get('/table', 'HomeController@table');

Route::get('/data-tables', 'HomeController@datatable');

Route::get('/cast/create', 'CastController@create' );
Route::get('/cast', 'CastController@index' );
Route::post('/cast', 'CastController@store' );
Route::get('/cast/{cast_id}', 'CastController@show' );
Route::get('/cast/{cast_id}/edit', 'CastController@edit' );
Route::put('/cast/{cast_id}', 'CastController@update' );
Route::delete('/cast/{cast_id}', 'CastController@destroy' );