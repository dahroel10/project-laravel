<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function home(){
        return view('/home');
    }

    public function master(){
        return view('/adminlte/master');
    }

    public function table(){
        return view('/table');
    }

    public function datatable(){
        return view('/data-table');
    }
}
